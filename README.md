# installation
```
oc apply -f namespace.yaml
oc project tekton-test
kubectl create secret docker-registry regcred --docker-server=docker.io --docker-username=kamleitermichael --docker-password='PASSWORD'
oc apply -f serviceaccount.yaml
oc adm policy add-cluster-role-to-user cluster-admin -z firstpipeline
oc apply -f git.yaml
oc apply -f image.yaml
oc apply -f clustertask.yaml
oc apply -f clustertask2.yaml
oc apply -f pipeline.yaml
vim piplinerun.yaml
  change name of object
oc apply -f pipelinerun.yaml
```
